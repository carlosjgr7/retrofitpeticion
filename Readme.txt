Aplicacion Sencilla hecha con Retrofit

�Que se hizo?

- Se selecciono la api de https://jsonplaceholder.typicode.com
con el end_point: /posts.

- Se creo una interfaz "ServicesRetrofit" con un metodo @GET
necesario para la peticion.

- Se creo una clase Movies que representa el objeto Json al que estamos 
haciendo la peticion.

- en el MainActivity se creo una funcion loadJSON() en el que se:

1) se crea el objeto ConverterFactory gson 
2) se crea el objeto Retrofict y se le agg el convertidor al mismo
3) se crea un objeto ServicesRetrofit
4)por ultimo se hace el llamado al metodo correspondiente.