package com.example.carlosjavier.app_retrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private  RecyclerView recyclerView;
    private adapterRecycler adapterRecycler;
    private ArrayList<Movies> arraylist;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recycler);
        arraylist = new ArrayList<Movies>();
        layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(layoutManager);

        adapterRecycler = new adapterRecycler(arraylist,R.layout.title_movie,this);

        recyclerView.setAdapter(adapterRecycler);

        loadJSON();

    }

    private void loadJSON() {

        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        Retrofit retrofit = new  Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServicesRetrofit restClient = retrofit.create(ServicesRetrofit.class);

        restClient.getMoviesPost().enqueue(new Callback<List<Movies>>() {
            @Override
            public void onResponse(Call<List<Movies>> call, Response<List<Movies>> response) {


                for (Movies movie:response.body()) {
                  arraylist.add(movie);
                }
                adapterRecycler.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<List<Movies>> call, Throwable t) {
                Log.i("esto es unerror",call.toString());
            }
        });
    }
}

